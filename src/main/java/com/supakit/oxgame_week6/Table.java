/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supakit.oxgame_week6;

import java.io.Serializable;

/**
 *
 * @author SUPAKIT KONGKAM
 */
public class Table implements Serializable {

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner = null;
    private boolean finish = false;
    private int lastCol;
    private int lastRow;
    static int countPlayer = 0;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println(" ");
        }

    }

    public boolean setRowCol(int row, int col) {
        if (isFinish()) {
            return false;
        }
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            checkWin();
            checkDraw();
            return true;
        }
        return false;
    }

    public char getRowCol(int row, int col) {
        return table[row][col];
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
            countPlayer++;
        } else {
            currentPlayer = playerX;
            countPlayer++;
        }
    }

    public void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }

        setStatWinLose();

    }

    public void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        setStatWinLose();
    }

    public void checkX() {
        for (int rowcol = 0; rowcol < 3; rowcol++) {
            if (table[rowcol][rowcol] != currentPlayer.getName()) {
                return;
            }
        }
        setStatWinLose();

    }

    public void checkX2() {
        if (table[0][2] == currentPlayer.getName()
                && table[1][1] == currentPlayer.getName()
                && table[2][0] == currentPlayer.getName()) {
            setStatWinLose();
        }

    }

    public void setStatWinLose() {
        countPlayer = 0;
        finish = true;
        winner = currentPlayer;
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else if (currentPlayer == playerX) {
            playerX.win();
            playerO.lose();
        }

    }

    public void checkDraw() {
        if (countPlayer == 8 && winner == null) {
            countPlayer = 0;
            winner = null;
            finish = true;
        }
    }

    public void checkWin() {
        checkCol();
        checkRow();
        checkX();
        checkX2();
    }

    public void clearTableNewGame() {
        countPlayer = -1;
        winner = null;
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }

    public void setCountPlayer() {
        this.countPlayer = 0;
    }

}
